#######################
#Author: Dennis Rich (denrich@stanford.edu)
#April 22, 2024
######################

from dataclasses import dataclass

@dataclass
class ThermalIC:
	"""1D thermal model"""
	system: list #list of layers and sources, with heatsink at index 0
	ambient_T: float = 25 #degrees C

	def calculate_peak_temp(self) -> float:
		resistance_below = 0
		peak_temp = self.ambient_T
		for layer in self.system:
			if isinstance(layer,Layer):
				resistance_below += 1/layer.get_htc()
			elif isinstance(layer,Source):
				peak_temp += resistance_below*layer.P
		return peak_temp

@dataclass
class Layer:
	"""layer behaving as resistor in the 1D thermal model"""
	htc: float=None #W/m2/K. overrides h and k HTC calculation
	h: float=None #thickness of material in um.
	k: float=None #thermal conductivity of material in W/m/K. 
	
	def get_htc(self) -> float:
		"""get the heat transfer coefficient in W/m2/K of the layer"""
		if self.htc:
			return self.htc
		else:
			return self.k/(self.h*1e-6)

@dataclass
class Source:
	"""layer of heat sources (e.g., compute devices) behaving as current source in the 1D thermal model"""
	P_Wcm2: float #power in W/cm2

	def __post_init__(self):
		self.P = self.P_Wcm2*1e4 #power in W/m2	

if __name__=="__main__":
	heatsink = Layer(htc=3e4) #value from J.-N. Hung et al., ECTC 2021
	tim = Layer(h=100,k=10)
	si_handle = Layer(h=500,k=150)
	beol = Layer(h=1,k=1)
	hybrid_bond = Layer(htc=1e6)
	compute = Source(P_Wcm2=75)
	mem = Source(P_Wcm2=5)

	system = [heatsink,tim,si_handle,compute,beol,hybrid_bond,si_handle,mem]

	my_ic=ThermalIC(ambient_T=25,system=system)
	print(f"Peak temperature: {my_ic.calculate_peak_temp():.2f} degC")
